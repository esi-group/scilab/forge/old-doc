% Copyright INRIA

\chapter{Data Types}
\label{ch2}
\index{data types}
Scilab recognizes several data types.
 Scalar objects are numerical constants, booleans, polynomials, strings and 
rationals (quotients of polynomials). These objects in turn allow to 
define matrices which admit these scalars as entries.
Other basic objects are lists, typed-lists and functions.
%
The objective of this chapter is to describe the use of each of 
these data types.

\section{Special Constants}
\label{s2.1}
\index{data types!constants}
\index{constants}

	Scilab provides special constants {\tt \%i}, {\tt \%pi},
{\tt \%e}, and {\tt \%eps} as primitives.  The {\tt \%i} constant
represents $\sqrt{-1}$, {\tt \%pi} is $\pi=3.1415927\cdots$ , {\tt \%e}
is the trigonometric constant $e=2.7182818\cdots$, and {\tt \%eps}
is a constant representing the precision of the machine ({\tt \%eps}
is the biggest number for which $1+\mbox{\tt \%eps}=1$). {\tt \%inf}
and {\tt \%nan} stand for ``Infinity'' and ``NotANumber'' respectively.
{\tt \%s} is the polynomial s={\tt poly(0,'s')} with symbol {\tt s}.

(More generally, given a vector {\tt rts}, {\tt p=poly(rts,'x')}
defines the polynomial p(x) with variable {\tt x} and such 
that {\tt roots(p)} = {\tt rts}).

Finally boolean constants are {\tt \%t} and {\tt \%f} which stand for
``true'' and ``false'' respectively. Note that {\tt \%t} is the
same as {\tt 1==1} and {\tt \%f} is the same as {\verb!~%t!}.

These variables are considered as ``predefined''. They are protected, cannot 
be deleted and are not saved by the {\tt save} command. It is possible
for a user to have his own ``predefined'' variables by using the
{\tt predef} command. The best way is probably to set these special variables
in his own startup file {\tt <home dir>/.scilab}.
Of course, the user can use e.g. {\tt i=sqrt(-1)} instead of {\tt \%i}.

\section{Constant Matrices}
\label{s2.2}
\index{data types!matrices}

	Scilab considers a number of data objects as matrices.  
Scalars and  vectors are all considered as matrices.  The details of the use
of these objects are revealed in the following Scilab sessions.

\paragraph{Scalars}\index{scalars}
Scalars are either real or complex numbers.  The values of
scalars can be assigned to variable names chosen by the user.
\begin{verbatim}
--> a=5+2*%i   //a complex number
 a         =
 
    5. + 2.i  
 
--> B=-2+%i;

--> b=4-3*%i
 b  =
 
    4. - 3.i 
 
--> a*b
 ans  =
 
    26. - 7.i  
 
-->a*B
 ans  =
 
  - 12. + i    
\end{verbatim}
Note that Scilab evaluates immediately lines that
end with a carriage return.  Instructions that ends with a semi-colon
are evaluated but are not displayed on screen. 

\paragraph{Vectors}\index{vectors}
The usual way of creating vectors is as follows, using 
commas (or blanks) and semi-columns:
\begin{verbatim}
 
--> v=[2,-3+%i,7]
 v         =
 
!   2.  - 3. + i      7. !
 
--> v'
 ans       =
 
!   2.       !
! - 3. - i   !
!   7.       !
 
--> w=[-3;-3-%i;2]
 w         =
 
! - 3.       !
! - 3. - i   !
!   2.       !
 
--> v'+w
 ans       =
 
! - 1.       !
! - 6. - 2.i !
!   9.       !
 
--> v*w
 ans       =
 
    18.  
 
--> w'.*v
 ans       =
 
! - 6.    8. - 6.i    14. !
\end{verbatim}
Notice that vector elements that are separated by commas (or by blanks)
yield row vectors and those separated by semi-colons give column
vectors. The empty matrix is \verb![]! ; it has zero rows and zero columns.
Note also that a single quote is used for transposing a 
vector\index{vectors!transpose} 
(one obtains the complex conjugate for complex entries).  Vectors of same
dimension can be added and subtracted.  The scalar product of a row and
column vector is demonstrated above.  Element-wise
multiplication ({\tt .*}) and division ({\tt ./}) is also possible 
as was demonstrated.

Note with the following example the role of the position of the blank:
\begin{verbatim}
-->v=[1 +3]
 v  =
 
!   1.    3. !
 
-->w=[1 + 3]
 w  =
 
!   1.    3. !
 
-->w=[1+ 3] 
 w  =
 
    4. 
 
-->u=[1, + 8- 7]
 u  =
 
!   1.    1. !

\end{verbatim}

	Vectors\index{vectors!incremental} of elements which increase
or decrease incrementely are constructed as follows 
\begin{verbatim}
 
--> v=5:-.5:3
 v         =
 
!   5.    4.5    4.    3.5    3. !
\end{verbatim}
The resulting vector begins with the first value and ends
with the third value stepping in increments of the second value.
When not specified the default increment is one.  A constant vector
can be created using the {\tt ones}\index{ones@{\tt ones}}\index{vectors!constant} and {\tt zeros} facility 
\begin{verbatim}
 
--> v=[1 5 6]
 v         =
 
!   1.    5.    6. !
 
--> ones(v)
 ans       =
 
!   1.    1.    1. !
 
--> ones(v')
 ans       =
 
!   1. !
!   1. !
!   1. !
 
--> ones(1:4)
 ans       =
 
!   1.    1.    1.    1. !
 
--> 3*ones(1:4)
 ans       =
 
!   3.    3.    3.    3. !

-->zeros(v)
 ans  =
 
!   0.    0.    0. !
 
-->zeros(1:5)
 ans  =
 
!   0.    0.    0.    0.    0. !
 
\end{verbatim}
Notice that {\tt ones} or {\tt zeros} replace its vector argument by a vector 
of equivalent dimensions filled with ones or zeros.

\paragraph{Matrices}\index{matrices}\index{data types!matrices}\label{Matrices}
Row elements are separated by commas or spaces
 and column elements by semi-colons.  Multiplication
of matrices by scalars, vectors, or other matrices is in the usual
sense.  Addition and
subtraction of matrices is element-wise and element-wise
multiplication and division can be accomplished with the {\tt .*}
and {\tt ./} operators.
\begin{verbatim}
 
--> A=[2 1 4;5 -8 2]
 A         =
 
!   2.    1.    4. !
!   5.  - 8.    2. !
 
--> b=ones(2,3)
 b         =
 
!   1.    1.    1. !
!   1.    1.    1. !
 
--> A.*b
 ans       =
 
!   2.    1.    4. !
!   5.  - 8.    2. !
 
--> A*b'
 ans       =
 
!   7.    7. !
! - 1.  - 1. !
\end{verbatim}
Notice that the {\tt ones}\index{ones@{\tt ones}}\index{matrices!constant}
operator with two real numbers as arguments separated
by a comma creates a matrix of ones using the arguments as
dimensions (same for {\tt zeros}).
Matrices can be used as elements to larger 
matrices\index{matrices!block construction}.  Furthermore,
the dimensions of a matrix can be changed.
\begin{verbatim}
 
--> A=[1 2;3 4]
 A         =
 
!   1.    2. !
!   3.    4. !
 
--> B=[5 6;7 8];
 
--> C=[9 10;11 12];
 
--> D=[A,B,C]
 D         =
 
!   1.    2.    5.    6.    9.     10. !
!   3.    4.    7.    8.    11.    12. !
 
--> E=matrix(D,3,4)
 E         =
 
!   1.    4.    6.    11. !
!   3.    5.    8.    10. !
!   2.    7.    9.    12. !

-->F=eye(E)
 F  =
 
!   1.    0.    0.    0. !
!   0.    1.    0.    0. !
!   0.    0.    1.    0. !
 
-->G=eye(4,3)
 G  =
 
!   1.    0.    0. !
!   0.    1.    0. !
!   0.    0.    1. !
!   0.    0.    0. !

\end{verbatim}
Notice that matrix {\tt D} is created by using other matrix
elements.  The {\tt matrix}\index{matrix@{\tt matrix}} 
primitive creates a new matrix {\tt E} with the elements of the
matrix {\tt D} using
the dimensions specified by the second two arguments.  The element
ordering in the matrix {\tt D} is top to bottom and then left to right
which explains the ordering of the re-arranged matrix in {\tt E}.

The function {\tt eye} creates an $m\times n$ matrix with 1 along the main
diagonal (if the argument is a matrix {\tt E} , $m$ and $n$ are the 
dimensions of {\tt E} ) .

Sparse constant matrices are defined through their nonzero entries 
(type help {\tt sparse} for more details). Once defined, they are
manipulated as full matrices.


\section{Matrices of Character Strings}
\label{s2.3}
\index{data types!character strings}
\index{character strings}

Character strings can be created by using single or double quotes.
Concatenation of strings is performed by the {\tt +} operation.
Matrices of character strings are constructed as ordinary matrices,
e.g. using brackets.  An important feature of matrices of
character strings is the capacity to manipulate and create functions.
Furthermore, symbolic manipulation of mathematical objects can be
implemented using matrices of character strings.  The following
illustrates some of these features.
\begin{verbatim}
 
--> A=['x' 'y';'z' 'w+v']
 A         =
 
!x  y    !
!        !
!z  w+v  !
 
--> At=trianfml(A)
 At        =
 
!z  w+v          !
!                !
!0  z*y-x*(w+v)  !
 
--> x=1;y=2;z=3;w=4;v=5;
 
--> evstr(At)
 ans       =
 
!   3.    9. !
!   0.  - 3. !

\end{verbatim}
Note that in the above Scilab session the function 
{\tt trianfml}\index{trianfml@{\tt trianfml}}\index{symbolic triangularization}
performs the symbolic triangularization of the matrix {\tt A}.
The value of the resulting symbolic matrix can be obtained by
using {\tt evstr}.

The following table gives the list of some useful functions:

\begin{center}
\begin{tabular}{|c|c|}
\hline
\verb!ascii! & ascii code of strings\\ \hline

\verb!execstr! & executes the string\\ \hline

\verb!grep ! & looks for a chain into a matrix \\ \hline

\verb!sort, gsort ! & sorting (lexicographic,...) \\ \hline

\verb!part! & extract a subchain  \\ \hline

\verb!mmscanf! & formated read into a chain \\ \hline

\verb!msprintf! & construct a chain/vector   \\ \hline

\verb!strindex! & location of a subchain \\ \hline

\verb!string ! & convert into a string \\ \hline

\verb!stripblanks! & remove blank characters \\ \hline

\verb!strsubst! & chain replacement \\ \hline

\verb!tokens! &  cuts a chain \\ \hline

\verb!strcat! & catenates chains  \\ \hline

\verb!length! &  chain length  \\ \hline
\end{tabular}
\end{center}


	A string matrices can be used to create new 
functions (for more on functions
see Section~\ref{s4.2}).  An example of automatically creating a 
function is illustrated in the following Scilab session where it is
desired to study a polynomial of two variables {\tt s} and {\tt t}.
Since polynomials in two independent variables are not directly 
supported in Scilab, we can construct a new data structure using
a list (see Section~\ref{s2.5}).
The polynomial to be studied is $(t^2+2t^3)-(t+t^2)s+ts^2+s^3$.
\begin{verbatim}
-->s=poly(0,'s');t=poly(0,'t');
 
-->p=list(t^2+2*t^3,-t-t^2,t,1+0*t);
 
-->pst=makefunction(p) //pst is a function 
//                t->p (number->polynomial)
 pst       =
 
[p]=pst(t)
 
-->pst(1)
 ans       =
 
              2   3  
    3 - 2s + s + s   
\end{verbatim}
Here the polynomial is represented by the command which puts
the coefficients of the variable {\tt s} in the list {\tt p}.
The list {\tt p} is then processed by the function {\tt makefunction}
which makes a new function {\tt pst}.  The contents of the new function
can be displayed and this function can be evaluated
at values of {\tt t}.  The creation of the new function {\tt pst}
is accomplished as follows

\input{macros/make_macro.vtex}

Here the function {\tt makefunction} takes the list {\tt p} and creates the
function {\tt pst}.  Inside of {\tt makefunction} there is a call to another 
function {\tt makestr} which makes the string which represents each 
term of the new two variable polynomial.  The functions {\tt addf} and
{\tt mulf} are used for adding and multiplying strings (i.e. 
{\tt addf(x,y)} yields the string {\tt x+y}).  Finally, the 
essential command for creating the new function 
is the primitive {\tt deff}.  The {\tt deff} primitive 
creates a function defined by two matrices
of character strings.  Here the 
function {\tt p} is defined by the two character strings
{\tt '[p]=newfunction(t)'} and {\tt text} where the string {\tt text}
evaluates to the polynomial in two variables.

\section{Polynomials and Polynomial Matrices}
\label{s2.4}
\index{data types!polynomials}
\index{polynomials}

	Polynomials are easily created and manipulated in Scilab.
Manipulation of polynomial matrices is essentially identical
to that of constant numerical matrices.
The {\tt poly}\index{poly@{\tt poly}} 
primitive in Scilab can be used to specify the coefficients
of a polynomial or the roots of a polynomial.
\begin{verbatim}
-->p=poly([1 2],'s')   //polynomial defined by its roots
 p         =
 
              2  
    2 - 3s + s   
 
-->q=poly([1 2],'s','c')  //polynomial defined by its coefficients
 q         =
 
    1 + 2s   
 
-->p+q
 ans       =
 
             2  
    3 - s + s   
 
-->p*q
 ans       =
 
              2    3  
    2 + s - 5s + 2s   
 
--> q/p
 ans       =
 
      1 + 2s     
    -----------  
              2  
    2 - 3s + s   
\end{verbatim}
Note that the polynomial {\tt p} has the {\em roots}
1 and 2 whereas the polynomial {\tt q} has the {\em coefficients}
1 and 2.  It is the third argument in the {\tt poly} primitive which
specifies the coefficient flag option.  
In the case where the first argument of {\tt poly} is a square matrix
and the roots option is in effect the result is the characteristic
polynomial of the matrix.
\begin{verbatim}
 
--> poly([1 2;3 4],'s')
 ans       =
 
              2  
  - 2 - 5s + s   
\end{verbatim}
Polynomials can be added,
subtracted, multiplied, and divided, as usual, but only between polynomials
of same formal variable.

	Polynomials, like real and complex constants, can be used
as elements in matrices.  This is a very useful feature of Scilab
for systems theory.
\begin{verbatim}
 
-->s=poly(0,'s');
 
-->A=[1 s;s 1+s^2];   //Polynomial matrix
 
--> B=[1/s 1/(1+s);1/(1+s) 1/s^2]
 B         =
 
!   1           1    !
! ------      ------ !
!   s         1 + s  !
!                    !
!     1       1      !
!    ---     ---     !
!              2     !
!   1 + s     s      !

\end{verbatim}
From the above examples it can be seen that matrices can be constructed
from polynomials and rationals.

\subsection{Rational polynomial simplification}
Scilab automatically performs pole-zero simplifications when the 
the built-in primitive {\tt simp} finds a common factor in the
numerator and denominator of a rational polynomial {\tt num/den}.
Pole-zero simplification is a difficult problem from a
numerical viewpoint and {\tt simp} function is usually conservative.
When making calculations with polynomials, it is sometimes desirable
to avoid pole-zero simplifications: this is possible by switching
Scilab into a ``no-simplify'' mode: \verb!help simp_mode!. The
function {\tt trfmod} can also be used for simplifying specific
pole-zero pairs.


\section{Boolean Matrices}
\index{data types!booleans}
\index{boolean}

Boolean constants are {\tt \%t} and {\tt \%f}. They can be used in
boolean matrices. The syntax is the same as for ordinary matrices i.e.
they can be concatenated, transposed, etc...

Operations symbols used with boolean matrices or used to create
boolean matrices are {\tt ==} and \verb!~!. 

If {\tt B} is a matrix of booleans {\tt or(B)} and {\tt and(B)} 
perform the logical {\tt or} and {\tt and}.
\begin{verbatim}
-->%t
 %t  =
 
  T  
 
-->[1,2]==[1,3]
 ans  =
 
! T F !
 
-->[1,2]==1
 ans  =
 
! T F !
 
-->a=1:5; a(a>2)
 ans  =
 
!   3.    4.    5. !

-->A=[%t,%f,%t,%f,%f,%f];   
 
-->B=[%t,%f,%t,%f,%t,%t]
 B  =
 
! T F T F T T !
 
-->A|B
 ans  =
 
! T F T F T T !

-->A&B
 ans  =
 
! T F T F F F !
 
\end{verbatim} 

Sparse boolean matrices are generated when, e.g., two constant sparse
matrices are compared. These matrices are handled as ordinary boolean
matrices.


%%%%%
\section{Integer Matrices}
\index{data types!integer}
\index{integer}

There are 6 integer data types defined in Scilab, all these types have
the same major type (see the {\tt type} function) which is 8  and
different sub-types (see the {\tt inttype}\index{inttype@{\tt inttype}} function)
\begin{itemize}
\item 32 bit signed integers (sub-type 4)
\item 32 bit unsigned integers (sub-type 14)
\item 16 bit signed integers (sub-type 2)
\item 16 bit unsigned integers (sub-type 23)
\item 8 bit signed integers (sub-type 2)
\item 8 bit unsigned integers (sub-type 12)
\end{itemize}
It is possible to build these integer data types from standard matrix
(see \ref{Matrices}) using the {\tt int32}\index{int32@{\tt int32}}, {\tt uint32}\index{uint32@{\tt uint32}}, {\tt int16}\index{int16@{\tt int16}},
{\tt uint16}\index{uint16@{\tt uint16}}, {\tt int8}\index{int8@{\tt
int8}}, {\tt uint8}\index{uint8@{\tt uint8}} 
conversion functions
\begin{verbatim}
-->x=[0 3.2 27 135] ;

-->int32(x)
 ans  =
 
!0   3  27  135 !

-->int8(x)
 ans  =
 
!0   3  27  -121!
-->uint8(x)
 ans  =
 
!0   3  27  135 !
\end{verbatim}

The same function can also convert from one sub-type to another
one. The {\tt double} function transform any of the integer type in a
standard type:

\begin{verbatim}

-->y=int32([2 5 285])
 y  =
 
!2  5  285 !

-->uint8(y)
 ans  =
 
!2  5  29 !

-->double(ans)
 ans  =
 
!   2.    5.    29. !
\end{verbatim}

Arithmetic and comparison operations can be applied to this type

\begin{verbatim}

-->x=int16([1 5 12])
 x  =
 
!1  5  12 !

-->x([1 3])
 ans  =
 
!1  12 !
 
-->x+x

 ans  =
 
!2  10  24 !

-->x*x'             
 ans  =
 
 170
-->y=int16([1 7 11])
 y  =
 
!1  7  11 !
-->x>y
 ans  =
 
! F F T !
\end{verbatim} 

The operators \verb!&!, {\tt |} and {\tt ~} used with these datatypes correspond to
AND, OR and NOT bit-wise operations. 


\begin{verbatim}
-->x=int16([1 5 12])
 x  =
 
!1  5  12 !
 
-->x|int16(2)
 ans  =
 
!3  7  14 !

-->int16(14)&int16(2)
 ans  =
 
 2  
-->~uint8(2)
 ans  =
 
 253
\end{verbatim}
%%%%%
\section{N-dimensionnal arrays}
N-dimensionnal array can be defined and handled in simple way:
\begin{verbatim}
-->M(2,2,2)=3
 M  =
 
(:,:,1)
 
!   0.    0. !
!   0.    0. !
(:,:,2)
 
!   0.    0. !
!   0.    3. !
 
-->M(:,:,1)=rand(2,2)
 M  =
 
(:,:,1)
 
!   0.9329616    0.312642  !
!   0.2146008    0.3616361 !
(:,:,2)
 
!   0.    0. !
!   0.    3. !
 
-->M(2,2,:)
 ans  =
 
(:,:,1)
 
    0.3616361  
(:,:,2)
 
    3.  
-->size(M)
 ans  =
 
!   2.    2.    2. !
 
-->size(M,3)
 ans  =
 
    2.  
 
\end{verbatim}

They can be created from a vector of data and a vector of dimension 

\begin{verbatim}
-->hypermat([2 3,2],1:12)
 ans  =
 
(:,:,1)
 
!   1.    3.    5. !
!   2.    4.    6. !
(:,:,2)
 
!   7.    9.     11. !
!   8.    10.    12. !
 
\end{verbatim}

N-dimensionnal matrices are coded as {\tt mlists} with 2 fields~:
\begin{verbatim}
-->M=hypermat([2,3,2],1:12);
-->M.dims
 ans  =
 
!   2.    3.    2. !
-->M.entries'
ans  =
         column  1 to 11
 
!   1.    2.    3.    4.    5.    6.    7.    8.    9.    10.    11. !
 
         column 12
 
!   12. !
\end{verbatim}


\section{Lists}
\label{s2.5}
\index{data types!lists}
\index{lists}

	Scilab has a list data type.  The list is a collection of data
objects not necessarily of the same type.  A list can contain any of
the already discussed data types (including functions) as well as
other lists.  Lists are useful for defining structured data objects.

There are two kinds of lists, ordinary lists and typed-lists.
A list is defined by the {\tt list} function. Here is a simple
example:

\begin{verbatim}
-->L=list(1,'w',ones(2,2))  //L is a list made of 3 entries
 L  =
       L(1)
 
    1.  
       L(2)
 w   
       L(3)
 !   1.    1. !
!   1.    1. !

-->L(3);   //extracting entry 3 of list L

-->L(3)(2,2)  //entry 2,2 of matrix L(3)
 ans  =
 
    1.  

-->L(2)=list('w',rand(2,2)) //nested list: L(2) is now a list
 L  =
       L(1)
 
    1.  
       L(2)
        L(2)(1)
 w   
        L(2)(2)
!   0.6653811    0.8497452 !
!   0.6283918    0.6857310 !
       L(3)
!   1.    1. !
!   1.    1. !

-->L(2)(2)(1,2)  //extracting entry 1,2 of entry 2 of L(2)
 ans  =
 
    0.8497452  

-->L(2)(2)(1,2)=5; //assigning a new value to this entry.

\end{verbatim}

Typed lists have a specific first entry. This first entry must be a 
character string (the type) or a vector of character string (the first
component is then the type, and the following elements the names given
to the entries of the list). Typed lists entries can be manipulated
by using character strings (the names) as shown below.

\begin{verbatim}
-->L=tlist(['Car';'Name';'Dimensions'],'Nevada',[2,3])
 L  =
       L(1)
!Car         !
!            !
!Name        !
!            !
!Dimensions  !
 
       L(2)
 Nevada   
       L(3)
!   2.    3. !

-->L.Name    //same as L(2)
 ans  =
 Nevada   
-->L.Dimensions(1,2)=2.3

 L  =
       L(1)
 
!Car         !
!            !
!Name        !
!            !
!Dimensions  !
 
       L(2)
 Nevada   
       L(3)
 
!   2.    2.3 !

-->L(3)(1,2)
 ans  =
 
    2.3  

-->L(1)(1)
 ans  =
 
 Car
\end{verbatim}
An important feature of typed-lists is that it is possible to define
operators acting on them (overloading), i.e., it is possible
to define e.g. the multiplication \verb!L1*L2! of the two typed lists 
\verb!L1! and \verb!L2!. 


\section{Functions}
\label{s2.6}
\index{data types!functions}
\index{functions}

 Functions are collections of commands which are executed in a
new environment thus isolating function variables from the original
environments variables.  Functions
can be created and executed in a number of different ways.
Furthermore, functions can pass arguments, have programming features
such as conditionals and loops, and can be recursively called.
Functions can be arguments
to other functions and can be elements in lists.  The most useful
way of creating functions is by using a text editor, however, functions
can be created directly in the Scilab environment using the syntax
{\tt function}\index{function@{\tt function}} or the
{\tt deff}\index{deff@{\tt deff}}\index{functions!{\tt deff}} primitive.
\begin{verbatim}
--> function [x]=foo(y)
-->    if y>0 then, x=1; else, x=-1; end
--> endfunction
 
--> foo(5)
 ans       =
 
    1.  
 
--> foo(-3)
 ans       =
 
  - 1.  
\end{verbatim}
Usually functions are defined in a file using an editor and loaded
into Scilab with:\\
{\tt exec('filename')}.\\
This can be done also by clicking in the {\tt File operation} button.
This latter syntax loads the function(s) in {\tt filename} and compiles
them.
The first line of {\tt filename} must be as follows:
\begin{verbatim}
function [y1,...,yn]=macname(x1,...,xk)
\end{verbatim}
where the {\tt yi}'s are output variables and the {\tt xi}'s the
input variables.

For more on the use and creation of functions see Section~\ref{s4.2}.

\section{Libraries}
\label{s2.7}
\index{data types!libraries}
\index{libraries}

	Libraries are collections of functions which can be either 
automatically loaded into the Scilab environment when
Scilab is called, or loaded when desired by the user.  
Libraries are created by the {\tt lib} command. Examples of librairies
are given in the {\tt SCIDIR/macros} directory. Note that in these
directory there is an ASCII file ``names'' which contains the names
of each function of the library, a set of {\tt .sci} files which
contains the source code of the functions and a set of {\tt .bin} files
which contains the compiled code of the functions. The Makefile invokes
{\tt scilab} for compiling the functions and generating the {\tt .bin}
files. The compiled functions of a library are automatically loaded 
into Scilab at their first call. To build a library the command {\tt
genlib} can be used (\verb!help genlib!).

\section{Objects}
We conclude this chapter by noting that the function {\tt typeof}
returns the type of the various Scilab objects. The following objects
are defined:
\begin{itemize}
\item{\tt usual} 	for matrices with real or complex entries.
\item{\tt polynomial} 	for polynomial matrices: coefficients can be 
real or complex.
\item{\tt boolean} 	for boolean matrices.
\item{\tt character} 	for matrices of character strings.
\item{\tt function} 	for functions.
\item{\tt rational} 	for rational matrices ({\tt syslin} lists)
\item{\tt state-space} 	for linear systems in state-space 
form ({\tt syslin} lists).
\item{\tt sparse} 	for sparse constant matrices (real or complex)
\item{\tt boolean sparse} for sparse boolean matrices.
\item{\tt list} 	for ordinary lists.
\item{\tt tlist}        for typed lists.
\item{\tt mlist}        for matrix oriented typed lists.
\item{\tt state-space (or rational)} for syslin lists.
\item{\tt library} 	for library definition.
\end{itemize}


\section{Matrix Operations}
The following table gives the syntax of the basic matrix operations
available in Scilab.

\begin{center}
\begin{tabular}{|c|c|}
\hline
SYMBOL & OPERATION 
\\ \hline \hline
\verb![ ]! & matrix definition, concatenation\\ \hline

\verb!;! & row separator\\ \hline

\verb!( )! & extraction \verb!m=a(k)! \\ \hline

\verb!( )! & insertion:  \verb!a(k)=m!  \\ \hline

\verb!.'! & transpose \\ \hline

\verb!'! & conjugate transpose \\ \hline

\verb!+! & addition  \\ \hline

\verb!-! & subtraction \\ \hline

\verb!*! & multiplication \\ \hline

\verb!\! & left division \\ \hline

\verb!/! & right division \\ \hline

\verb!^! &  exponent \\ \hline

\verb!.*! & elementwise multiplication  \\ \hline

\verb!.\! &  elementwise left division  \\ \hline

\verb!./! &  elementwise right division  \\ \hline

\verb!.^! &  elementwise exponent  \\ \hline

\verb!.*.! & kronecker product \\ \hline

\verb!./.! & kronecker right division \\ \hline

\verb!.\.! &  kronecker left division \\ \hline  
\end{tabular}
\end{center}

\section{Indexing}

The following sample sessions shows the flexibility which is offered
for extracting and inserting entries in matrices or lists.
For additional details enter \verb!help extraction! 
or \verb!help insertion!.
\subsection{Indexing in matrices}
Indexing in matrices can be done by giving the indices of selected
rows and columns or by boolean indices or by using the \verb!$! symbol.
Here is a sample of commands: 
\input{diary/extract.dia}

\subsection{Indexing in lists}
The following session illustrates how to create lists and
insert/extract entries in {\tt list} and {\tt tlist} or {\tt mlist}.
Enter {\tt help insertion} and {\tt help extraction} for additinal examples.
Below is a sample of commands:
\input{diary/list.dia}

\subsection{Structs and Cells \`a la Matlab}
The command {\tt X=tlist(...)} or {\tt Y=mlist(...)} creates a
Scilab variable X of type {\tt tlist} or {\tt mlist}.
The entries of {\tt X} are obtained by the names of their fields.

For instance, if {\tt X=tlist(['mytype','a','b'],A,B)} the command
X.a returns A. If A is a matrix the command X.a(2,3) returns the 
entry at row 2 and column 3 of X;a, i.e. {\tt A(2,3)}.
Similarly, if {\tt Y=mlist(['mytype','a','b'],A,B)}, we can use the
syntax Y(2,3), once the extraction function \verb! %mytype_e(varargin)!
has been defined. 

Also the syntax {\tt Y(2,3)=33} can be given a meaning
through the function \verb!%s_i_mytype(varargin)!. 
This powerful overloading mechanism allows to define complex objects
with a general indexing where indices can be fields (string) or a set
of integers.

If the variable X is not defined in the Scilab workspace, then
the command X.a=A creates a particular mlist which behaves as 
a Matlab struct. Its internal representation is similar to
{\tt X=mlist(['st','dims','a'],int32([1,1]),A)}.
It is a one dimensional {\tt struct} with one field called 'a' and 
the value of X.a is A.
Multidimensional structs are created and manipulated in a similar way.
For instance {\tt X(2,3).a.b(2)=4} creates a 2 x 3 struct with one field
a. It is represented as
\begin{verbatim}
mlist(['st','dims','a'],int32([2,3]),list([],[],[],[],[],w))
\end{verbatim}
where w is a struct with one field 'b', and w.b is the vector [0,4].
A particular display is done for structs. Here, we have:
\begin{verbatim}
-->X(2,3)
 ans  =
 
   a: [1x1 struct]
 
-->X(2,3).a
 ans  =
 
   b: [0;4]
\end{verbatim}

All the struct manipulations are implemented by soft coded operations
i.e. Scilab overloaded functions. As structs are not basic data types
some operations are slow. They have been implemented for a better 
Matlab compatibility.

The Matlab cells are also easily emulated. A cell is seen as a particular
struct with one field called 'entries'. We just show a simple example:

\begin{verbatim}

-->X=cell(1,2)
 X  =
 
!{}  {}  !

-->X(2).entries=11
 X  =
 
!{}  !
!    !
!11  !
\end{verbatim}

Note that Matlab uses braces \verb!X{2}! for extracting entries from
a cell. 
