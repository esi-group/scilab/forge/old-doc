\chapter{Scilab statistical features}

In this chapter, we describe the features which are provided 
in Scilab's core, that is, "out of the box".
Indeed, Scilab provide features such as general statistical 
description of datas, many cumulated density functions 
and can generate uniform and non uniform random variates.
These features are based on several open source libraries, that 
we are analysing in the first section.
A complete overview of these features is provided in the 
second section, where we analyse the full list of functions and 
the numerical methods they use. For the most important 
functions, we provide a sample session where the function is 
used and some plots of the results.

\section{The sources}

In this section, we analyse the libraries which are available 
in Scilab and which provide its statistical features.
The figure \ref{inscilab-libraries} is an overview of the 
libraries which are either Scilab macros or source code, provided
in C, Fortran 77 or as Scilab macros. 

\begin{figure}[htbp]
\begin{tabular}{|l|l|}
\hline
Commands & calerf, erf, erfc, erfcx \\
Routines & CALERF\\
Directory & scilab/modules/elementary\_functions/src/fortran\\
Language & Fortran\\
Download & \url{http://www.kurims.kyoto-u.ac.jp/~ooura/index.html} \\
Author & Takuya Ooura \\
Year & 1996 \\
References & \cite{Algorithm715} \\
\hline
\hline
Name & Labostat \\
Directory & scilab/modules/elementary\_functions/src/fortran\\
Commands & General description functions (center, variance, etc...) \\
Language & Scilab scripts \\
Author & Carlos Klimann \\
Year & 2000 \\
References & \cite{Wonacott1990}, \cite{Saporta2006}\\
\hline
\hline
Name & DCDFLIB \\
Directory & scilab/modules/statistics/src/dcdflib\\
Download & \url{http://www.netlib.org/random/}\\
Commands & Cumulated Density Functions (cdfbet, cdfbin, etc...)  \\
Language & Fortran \\
Author & Barry Brown, W. J. Cody, Alfred H. Morris Jr \\
Year & 1994 for library, 1992 for code by Cody, 1991 for code by Morris \\
References & \cite{abramowitz+stegun1964}, \cite{HartEtAl:1968}, \cite{Algorithm715}, \cite{Kennedy1980}
\cite{Algo708}, \cite{DiDonato1986}\\
\hline
\hline
Name & Randlib \\
Directory & scilab/modules/randlib/src/fortran\\
Download & \url{ftp://odin.mda.uth.tmc.edu/pub/source}\\
& (unavailable at the time of the writing of this report)\\
Commands & grand (for distributions like normal, gamma, chi, etc...)  \\
Language & Fortran \\
Author & Barry Brown, James Lovato, Kathy Russell, John Venier \\
Year & 1997 \\
References & \cite{Ahrens1972}, \cite{358390}, \cite{Devroye86non-uniformrandom},
\cite{AhrensDieter1973}\\
\hline
\end{tabular}
\caption{Statistical libraries available in Scilab}
\label{inscilab-libraries}
\end{figure}


\section{Overview of functions}

The figure \ref{inscilab-fulllist} presents a complete list 
of Scilab statistical functions.

\begin{figure}[htbp]
\begin{tabular}{|l|l|}
\hline
\textbf{Description} & \\
\textbf{of Data} & \\
\hline
center &     cmoment \\
correl &     covar  \\
ftest &     ftuneq  \\
geomean &    harmean  \\
iqr &    labostat  \\
mad &    mean  \\
meanf &    median  \\
moment &    msd  \\
mvvacov &    nancumsum  \\
nand2mean &    nanmax  \\
nanmean &    nanmeanf  \\
nanmedian &    nanmin  \\
nanstdev &    nansum  \\
nfreq &    pca  \\
perctl &    princomp  \\
quart &    regress  \\
sample &    samplef  \\
samwr &    show\_pca  \\
st\_deviation &    stdevf  \\
strange &    tabul  \\
thrownan &    trimmean  \\
variance &    variancef  \\
wcenter & \\
\hline
\end{tabular}
\begin{tabular}{|l|l|}
\hline
\textbf{Special} & \\
\textbf{Functions} & \\
\hline
beta & calerf \\
erf & erfc \\
erfcx & erfinv \\
gamma & gammaln \\
\hline
\hline
\textbf{Random} &\\
\textbf{Number} &\\
\textbf{Generation} &\\
\hline
grand & prbs\_a \\
rand & sprand \\
randpencil &\\
\hline
\hline
\textbf{Cumulated} &\\
\textbf{Density} &\\
\textbf{Functions} &\\
\hline
cdfbet & cdfbin  \\
cdfchi & cdfchn  \\
cdff & cdffnc  \\
cdfgam & cdfnbn  \\
cdfnor & cdfpoi  \\
cdft & \\
\hline
\end{tabular}
\caption{Complete list of statistical features in Scilab}
\label{inscilab-fulllist}
\end{figure}

\subsection{General description functions}

The figure \ref{inscilab-descriptionfunctions} presents the 
general description functions available in Scilab.

\begin{figure}[htbp]
\begin{tabular}{|l|l|}
\hline
Name & Feature\\
\hline
center & center \\
wcenter & center and weight \\
cmoment & central moments of all orders \\
correl & correlation of two variables \\
covar & covariance of two variables \\
ftest & Fischer ratio \\
ftuneq & Fischer ratio for samples of unequal size. \\
geomean & geometric mean \\
harmean & harmonic mean \\
iqr & interquartile range \\
mad & mean absolute deviation \\
mean & mean (row mean, column mean) of vector/matrix entries \\
meanf & weighted mean of a vector or a matrix \\
median & median (row median, column median,...) of vector/matrix/array entries \\
moment & non central moments of all orders \\
msd & mean squared deviation \\
mvvacov & computes variance-covariance matrix \\
nancumsum & cumulative sum of the values of a matrix \\
nand2mean & difference of the means of two independent samples \\
nanmax & max (ignoring Nan's) \\
nanmean & mean (ignoring Nan's) \\
nanmeanf & mean (ignoring Nan's) with a given frequency. \\
nanmedian & median of the values of a numerical vector or matrix \\
nanmin & min (ignoring Nan's) \\
nanstdev & standard deviation (ignoring the Nans). \\
nansum & sum of values ignoring Nan's \\
nfreq & frequence of the values in a vector or matrix \\
pca & computes principal components analysis with standardized variables \\
perctl & computation of percentils \\
princomp & Principal components analysis \\
quart & computation of quartiles \\
regress & regression coefficients of two variables \\
sample & sampling with replacement \\
samplef & sample with replacement from a population and frequences of his values. \\
samwr & sampling without replacement \\
show\_pca & visualization of principal components analysis results \\
st\_deviation & standard deviation (row or column-wise) of vector/matrix entries  \\
stdevf & standard deviation \\
strange & range \\
tabul & frequency of values of a matrix or vector \\
thrownan & eliminates nan values \\
trimmean & trimmed mean of a vector or a matrix \\
variance & variance of the values of a vector or matrix \\
variancef & standard deviation of the values of a vector or matrix \\
\hline
\end{tabular}
\caption{Description of Data functions}
\label{inscilab-descriptionfunctions}
\end{figure}

\subsection{Special functions}

The figure \ref{inscilab-specialfunctions} presents the special functions 
available in Scilab.

\begin{figure}[htbp]
\begin{tabular}{|l|l|}
\hline
Name & Feature\\
\hline
beta & beta function \\
calerf & computes error functions \\
erf & error function \\
erfc & complementary error function \\
erfcx & scaled complementary error function \\
erfinv & inverse of the error function \\
gamma & gamma function \\
gammaln & logarithm of gamma function \\
\hline
\end{tabular}
\caption{Special functions}
\label{inscilab-specialfunctions}
\end{figure}

The figure \ref{inscilab-specialfunctionsdetailed} presents 
a detailed analysis of the location and internal design
of the special functions available in Scilab.

\begin{figure}[htbp]
\begin{tabular}{|l|l|}
\hline
Name & Location / Internals \\
\hline
beta & modules/special\_functions/sci\_gateway/c/sci\_beta.c \\
& switch to dgammacody by W. J. Cody and \\
& L. Stoltz and to betaln from DCDFLIB \\
\hline
calerf & modules/elementary\_functions/src/fortran \\
& by Takuya OOURA \\
\hline
erf & modules/elementary\_functions/macros/erf.sci \\
&  call to calerf \\
\hline
erfc & modules/elementary\_functions/macros/erfc.sci \\
& call to calerf \\
\hline
erfcx & modules/elementary\_functions/macros/erfcx.sci \\
& call to calerf \\
\hline
erfinv & modules/special\_functions/macros/erfinv.sci \\
& rational aproximation of erfinv + 2 Newton's steps\\
\hline
gamma & modules/special\_functions/sci\_gateway/fortran/sci\_f\_gamma.f  \\
& based on dgammacody by W. J. Cody and L. Stoltz \\
\hline
gammaln & modules/elementary\_functions/src/fortran/dlgama.f  \\
& by W. J. Cody and L. Stoltz \\
\hline
\end{tabular}
\caption{Detailed analysis of special functions}
\label{inscilab-specialfunctionsdetailed}
\end{figure}

\subsection{Cumulated density functions}

The figure \ref{inscilab-cdffunctions} presents the cumulated
density functions available in Scilab.

\begin{figure}[htbp]
\begin{tabular}{|l|l|}
\hline
Name & Feature\\
\hline
cdfbet & Beta distribution \\
cdfbin & Binomial distribution \\
cdfchi & chi-square distribution \\
cdfchn & non-central chi-square distribution \\
cdff & F distribution \\
cdffnc & non-central F distribution \\
cdfgam & gamma distribution \\
cdfnbn & negative binomial distribution \\
cdfnor & normal distribution \\
cdfpoi & poisson distribution \\
cdft & Student's T distribution\\
\hline
\end{tabular}
\caption{Cumulated density functions}
\label{inscilab-cdffunctions}
\end{figure}


\subsection{Random number generation}

The figure \ref{inscilab-randomnumbercommands} presents the random
number generators available in Scilab.

\begin{figure}[htbp]
\begin{tabular}{|l|l|}
\hline
Name & Feature\\
\hline
grand & Random number generators \\
prbs\_a & pseudo random binary sequences generation \\
rand & random number generator \\
sprand & sparse random matrix \\
randpencil & random pencil \\
\hline
\end{tabular}
\caption{Random number commands}
\label{inscilab-randomnumbercommands}
\end{figure}

The figure \ref{inscilab-randomnumbercommands} presents a detailed 
analysis of the location and design of the random
number generators available in Scilab.

\begin{figure}[htbp]
\begin{tabular}{|l|l|}
\hline
Name & Location / Internals \\
\hline
grand & modules/randlib/sci\_gateway/c/sci\_grand.c \\
& based on several random number generators\\
prbs\_a & modules/cacsd/macros/prbs\_a.sci \\
& based on rand \\
rand & modules/elementary\_functions/src/fortran/urand.f \\
& by Michael A. Malcolm And Cleve B. Moler \\
sprand & (todo)\\
randpencil & (todo)\\
\hline
\end{tabular}
\caption{Detailed analysis of random number commands}
\label{inscilab-detailrandomnumbercommands}
\end{figure}

